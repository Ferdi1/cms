<?php
use Phalcon\Loader;
use Phalcon\Tag;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

//require "../app/controllers/ControllerBase.php";

try {
    // Register an autoloader
    $loader = new Loader();
    $loader->registerDirs(
        array(
            '../app/controllers/',
            '../app/models/',
        )
    )->register();
    // Create a DI
    $di = new FactoryDefault();
    // Set the database service
    $di['db'] = function() {
        return new DbAdapter(array(
            "host"     => "192.168.56.1",
            "username" => "mainuser",
            "password" => "dev01dev",
            "dbname"   => "CMS",
        ));
    };
//     Setting up the view component
    $di['view'] = function() {
        $view = new View();
        $view->setViewsDir('../app/views/');
        $view->registerEngines(
            [
                ".volt" => "Phalcon\\Mvc\\View\\Engine\\Volt",
            ]
        );
        return $view;
    };
    // Setup a base URI so that all generated URIs include the "tutorial" folder
    $di['url'] = function() {
        $url = new Url();
        $url->setBaseUri('');
        return $url;
    };
    // Setup the tag helpers
    $di['tag'] = function() {
        return new Tag();
    };
    // Handle the request
    $application = new Application($di);
    echo $application->handle()->getContent();

    $di['tag'] = function() {
        return new Tag();
    };
} catch (Exception $e) {
    echo "Exception: ", $e->getMessage();
}
