<?php

use Phalcon\Mvc\Controller;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Mvc\Model;
use Phalcon\Config;
use Phalcon\Mvc\Model\ResultsetInterface;

class ControllerBase extends Controller
{
    public function initialize()
    {

        $cntTxt = text::find(array('order'=>'cntID DESC', 'limit'=>1));

        foreach ($cntTxt as $cntID) {
            $this->view->cntTxt = $cntID->cntTxt;
        }


        $imageID = image::find(array("conditions" => "imgActive LIKE 1"));

        foreach ($imageID as $imageurl) {
            $values[] = $imageurl->imgUrl;
            $this->view->values = $values;
            $this->view->valuesLength = count($values) -1;
        }




        // ga gewoon verder met de layout en daarna technische kant zodat je precies weet wat je moet doen en als het kan een website ernaast
        // een tabel maken en een index maken, in de index kan je nieuwe newspost aanmaken en verwijderen ivm overzicht, als je op een nieuwspost klikt dan kan je hem aanpassen



        $this->view->baseUrl = $this->baseUrl;
    }



}