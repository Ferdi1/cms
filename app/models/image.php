<?php

use Phalcon\Mvc\Model;

class image extends Model {

    public $imgID;

    public $imgName;

    public $imgSize;

    public $imgUrl;


    public function getId() {
        return $this->imgID;
    }

    public function getName() {
        return $this->imgName;
    }

    public function getSize() {
        return $this->imgSize;
    }

    public function getUrl() {
        return $this->imgUrl;
    }

}