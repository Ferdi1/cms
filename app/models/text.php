<?php

use Phalcon\Mvc\Model;

class text extends Model {

    public $cntID;

    public $cntTit;

    public $cntPos;

    public $cntTxt;

    public function getId() {
        return $this->cntID;
    }

    public function getTit() {
        return $this->cntTit;
    }

    public function getPos() {
        return $this->cntPos;
    }

    public function getTxt() {
        return $this->cntTxt;
    }

}

