<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="../public/ckeditor/ckeditor.js"></script>
    <script src="../public/js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Anton|Bungee+Inline|Fjalla+One|Nunito|Open+Sans:400,800" rel="stylesheet">

    <meta charset="utf-8" />

    

    

</head>
<body>



    <div class="container-fluid">

        <div class="row">

            
            <div class="col-sm-12 header"></div>

        </div>

    </div>

    <div class="container-fluid">

        <div class="row">

            <ul class="nav nav-pills nav-stacked menu-ul">
                <li><a id="menu-ahref" href="text"><i class="fa fa-align-left fa-1x" aria-hidden="true"></i> Teksten <span></span></a></li>
                <li><a id="menu-ahref" href="image"><i class="fa fa-picture-o fa-1x" aria-hidden="true"></i> Afbeeldingen <span></span></a></li>
                <li><a id="menu-ahref" href="slider"><i class="fa fa-arrows-h fa-1x" aria-hidden="true"></i> Slider <span></span></a></li>
            </ul>

            <div class="col-sm-6 col-sm-offset-3">

                <?php echo $this->getContent(); ?>

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ex feugiat, ultricies augue vitae, rutrum est. Nulla vitae lorem ut enim egestas euismod. Nam non elementum tellus, ac suscipit lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse id ultricies est. Maecenas rhoncus felis quis porta molestie. Praesent tellus felis, ornare eu auctor in, dictum nec diam. Proin tristique velit ac elit efficitur, ultrices accumsan lorem pellentesque. Vivamus vel volutpat ligula.

                Proin ut lectus libero. In tempus, elit ut molestie accumsan, dolor odio venenatis nulla, et placerat nisl metus eget lorem. Donec a ultricies sem. Fusce euismod nisl sed mauris vestibulum, nec varius neque gravida. In et nisi vel metus fermentum pellentesque eu vitae felis. Fusce quis fermentum leo, nec pharetra ligula. Quisque in ex eget nunc imperdiet sagittis. Quisque in turpis ac orci dictum commodo. Donec eu magna et nisl tempor mollis. Duis eu luctus lacus.

                Duis porta est eros, eu semper arcu bibendum quis. Vestibulum placerat ex non ultricies posuere. Morbi scelerisque nec erat quis viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum in diam ac gravida. Sed venenatis tellus non iaculis pulvinar. Donec et mauris placerat, dignissim leo nec, pellentesque justo. Curabitur sodales viverra sapien. Nulla suscipit eget odio non rutrum.

                Aenean massa metus, luctus ut congue nec, tincidunt et eros. Curabitur imperdiet posuere diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras erat tortor, vulputate eget ante nec, pretium volutpat sapien. Curabitur pulvinar posuere mollis. Ut pretium, lorem eu consectetur semper, tellus ipsum sagittis elit, vel volutpat mi diam sit amet neque. Nam luctus enim nibh, elementum imperdiet sem vehicula sit amet. Nam pellentesque, dui vitae sodales blandit, lectus leo vulputate libero, at venenatis erat nisl eget elit. Nullam scelerisque sit amet tortor vitae vehicula. Curabitur sed placerat justo, ut dignissim felis.

                Cras justo magna, tempor nec metus accumsan, vestibulum euismod sem. Vestibulum elementum dolor eu nibh ultricies, in aliquet enim cursus. Aliquam erat volutpat. Nulla dignissim bibendum massa. Maecenas rhoncus mauris id augue sagittis, at elementum nunc fringilla. Nulla id justo at lacus faucibus dignissim. Sed sed lorem eu odio ultrices dapibus. Morbi quam massa, viverra gravida arcu in, condimentum iaculis ante. Integer nec congue est. Suspendisse quis diam auctor, suscipit nunc eget, iaculis erat. Nullam in finibus ipsum. Nulla pretium, lectus quis maximus malesuada, nisi lorem dignissim leo, at imperdiet ante turpis ac est. Quisque pretium tempor mi nec elementum. Sed porttitor diam quis orci accumsan pretium eget sit amet risus. In sagittis, massa vel aliquet ullamcorper, leo mauris dignissim lectus, eu vehicula risus lacus a orci. Fusce posuere erat ut nibh hendrerit mattis.

            </div>

        </div>

    </div>

    

    

<script>

// php files refreshen niet mee met <script> tag?? maar eerste keer wel ofzo wtf


$(document).ready(function() {

    var url = window.location;

    // Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().find('span').addClass('arrow-left');

});

</script>

    

</body>
</html>