{% extends "index.volt" %}

{% block content %}

    <form action="index/upload" method="post" enctype="multipart/form-data">
        <input type="file" name="files[]" multiple>
        <input type="submit" value="Send File(s)">
    </form>

    {{ textID }}

    {% for i in 0..valuesLength %}
        <img alt="image" style="width: 200px; height: 150px;" src="{{ baseUrl }} {{ values[i] }}"/>
    {% endfor %}

{% endblock %}